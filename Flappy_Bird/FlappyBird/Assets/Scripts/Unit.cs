﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour {

    public static Unit instance;

    Vector3 velocity = Vector3.zero;
    public float jumpForce = 8.0f;
    public float Speed = 7.5f;
    private Rigidbody2D rb;
    public BoxCollider2D[] Pipes;
    Animator animation;

    public int score = 0;
    bool hasStarted = false;
    bool isDead = false;

    void Awake() //to inherit
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    // Use this for initialization
    void Start () {
        // Caching
        rb = GetComponent<Rigidbody2D>();
        animation = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        if (hasStarted == false)
        {
            rb.velocity = new Vector2(0, Mathf.Sin(Time.time * 3)* 4);
        }

        if (isDead == false)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                hasStarted = true;
                rb.velocity = new Vector2(0, jumpForce);
                velocity.x = Speed;
            }

        }
        transform.position += velocity * Time.deltaTime;
    }

    void OnCollisionEnter2D (Collision2D col)
    {
        if (col.gameObject.tag == "Pipe")
        {
            Debug.Log("GAME OVER");
            isDead = true;
            
            for (int i = 0; i < Pipes.Length; i++)
            {
                Pipes[i].enabled = false;
            }
            rb.velocity = Vector2.zero;
            velocity = Vector3.zero;
            Speed = 0;
            animation.enabled = false;
        }
    }

    void OnTriggerEnter2D (Collider2D col)
    {
        if (col.gameObject.tag == "Scorer")
        {
            Debug.Log(score += 1);
        }
    }

    public bool Dead()
    {
        return isDead;
    }
    
    public bool Started()
    {
        return hasStarted;
    }
}
