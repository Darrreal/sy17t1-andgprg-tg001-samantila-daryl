﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour {

    public Transform playerToFollow;

    float offsetX;

    void Start ()
    {
        GameObject playerFlap = GameObject.FindGameObjectWithTag("Player");

        playerToFollow = playerFlap.transform;
        offsetX = transform.position.x - playerToFollow.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerToFollow != null)
        {
            Vector3 pos = transform.position;
            pos.x = playerToFollow.position.x + offsetX;
            transform.position = pos;
        }
    }
}
