﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public float spawnDelay = 2;
    public float posX = 1;
    private int spawnCount = 25;
    private int counter = 0;

    public List<GameObject> spawnedObjects;
    public List<GameObject> totalSpawned = new List<GameObject>();

    private GameObject spawnedObject1;
    private GameObject spawnedObject2;
    private GameObject spawnedObject3;
    private GameObject spawnedObject4;
    private GameObject spawnedObject5;


    // Use this for initialization
    void Start()
    {
        InvokeRepeating("Spawn", 0.0f, spawnDelay);
    }

    // Update is called once per frame  
    void Update()
    {
    }

    void Spawn()
    {
        float randPosY = Random.Range(0.0f, 10.0f);

        if (!Unit.instance.Dead() && totalSpawned.Count < spawnCount) //Spawn
        {
            spawnedObject1 = Instantiate(spawnedObjects[0], new Vector3(posX, 0.0f, 0.0f), Quaternion.identity);
            spawnedObject2 = Instantiate(spawnedObjects[1], new Vector3(posX, -13.5f, 0.0f), Quaternion.identity);
            spawnedObject3 = Instantiate(spawnedObjects[2], new Vector3(posX, 11.5f + randPosY, 0.0f), Quaternion.identity); // 21.5
            spawnedObject4 = Instantiate(spawnedObjects[3], new Vector3(posX, -17f + randPosY, 0.0f), Quaternion.identity); // -7
            spawnedObject5 = Instantiate(spawnedObjects[4], new Vector3(posX, -2.5f + randPosY, 0.0f), Quaternion.identity); // 7.5
            totalSpawned.Add(spawnedObject1);
            totalSpawned.Add(spawnedObject2);
            totalSpawned.Add(spawnedObject3);
            totalSpawned.Add(spawnedObject4);
            totalSpawned.Add(spawnedObject5);
        }

        if (!Unit.instance.Dead() && totalSpawned.Count >= spawnCount) //Replace
        {
            totalSpawned[counter].transform.position = new Vector3(posX, 0.0f, 0.0f);
            totalSpawned[counter + 1].transform.position = new Vector3(posX, -13.5f, 0.0f);
            totalSpawned[counter + 2].transform.position = new Vector3(posX, 11.5f + randPosY, 0.0f);
            totalSpawned[counter + 3].transform.position = new Vector3(posX, -17f + randPosY, 0.0f);
            totalSpawned[counter + 4].transform.position = new Vector3(posX, -2.5f + randPosY, 0.0f);

            counter += 5;
            if(counter > spawnCount - 5)
            {
                counter = 0;
            }
        }

        posX += 15.2f; //Next Spawn Position
    }
}
