﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HUDUIController : MonoBehaviour {

    public Text scoreText;
    public Text highScore;
    public GameObject restartButton;
    public int scoreValue;
    public Button button;
	// Use this for initialization
	void Start () {
        scoreText = GetComponent<Text>();
        button.onClick.AddListener(RestartGame);
        highScore.enabled = false;
        restartButton = GameObject.FindGameObjectWithTag("Button");
        restartButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        scoreValue = Unit.instance.score;
        scoreText.text = scoreValue.ToString();
        highScore.text = "SCORE: " + scoreValue.ToString();

        if (Unit.instance.Dead())
        {
           scoreText.enabled = false;
           highScore.enabled = true;
           restartButton.SetActive(true);
        }
    }

    public void RestartGame()
    {
        Debug.Log("Button Clicked");
        SceneManager.LoadScene("FlappyBird");
    }
}
