﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public static Player instance;
    public float playerHealth = 50.0f;
    float playerMaxHealth = 100.0f;
    bool isDead = false;

    void Awake() //to inherit
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Recover();
        poolHealth();
        checkHealth();

    }

    void Recover()
    {
        if (playerHealth < playerMaxHealth)
        {
            playerHealth += .001f;
            //Debug.Log("PLAYER HEALTH: " + playerHealth);
        }

        
    }

    void poolHealth()
    {
        if (playerHealth > playerMaxHealth)
        {
            playerHealth = playerMaxHealth;
        }
    }

    void checkHealth()
    {
        if (playerHealth < 0)
        {
            isDead = true;
            Destroy(gameObject);
            Time.timeScale = 0;
            Debug.Log("GAME OVER");
        }
    }
}
