﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Weapon : MonoBehaviour {

    public static Weapon instance;

    public GameObject projectilePrefab;
    private List<GameObject> Projectiles = new List<GameObject>();

    private float projectileVelocity;

    public LayerMask whatToHit;
    float timeToFire = 0;

    public float fireRate = 1.0f;
    public float bulletSpeed = 2.0f;
    public float bulletDamage = 10.0f;


    void Awake() //to inherit
    {
        if (instance == null)   
        {
            instance = this;
        }
    }

    void Start()
    {
  
    }

    void Update()
    {

        if (fireRate == 0)
        {
            Shoot();
        }

        else
        {
            if (Input.GetButtonDown("Fire1") && Time.time > timeToFire)
            {
                timeToFire = Time.time + 1 / fireRate;
                Shoot();
            }
        }
    }

    void Shoot()
    {
       
        if (Input.GetButtonDown("Fire1"))
        {
            Vector2 mouseLocation = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
            Vector2 playerLocation = new Vector2(transform.position.x, transform.position.y + 1);
            Vector2 bulletDirection = mouseLocation - playerLocation;
            bulletDirection.Normalize();
            Quaternion bulletRotationfx = Quaternion.Euler(0, 0, Mathf.Atan2(bulletDirection.y, bulletDirection.x) * Mathf.Rad2Deg);
            GameObject Bullet = Instantiate(projectilePrefab, playerLocation, bulletRotationfx);
            Bullet.GetComponent<Rigidbody2D>().velocity = bulletDirection * bulletSpeed;
            Debug.Log("SHOOT BULLET SPEED: " + bulletSpeed);
            Projectiles.Add(Bullet);
        }

        for (int i = 0; i < Projectiles.Count; i++)
        {
            GameObject bulletCount = Projectiles[i];
            if (bulletCount != null)
            {
                Vector3 bulletScreenPos = Camera.main.WorldToScreenPoint(bulletCount.transform.position);
                if (bulletScreenPos.y >= Screen.height + 5 || bulletScreenPos.y <= -5)
                {
                    DestroyObject(bulletCount);
                    Projectiles.Remove(bulletCount);
                }
                if (bulletScreenPos.x >= Screen.width + 5 || bulletScreenPos.x <= -5)
                {
                    DestroyObject(bulletCount);
                    Projectiles.Remove(bulletCount);
                }
            }
        }
    }
}
