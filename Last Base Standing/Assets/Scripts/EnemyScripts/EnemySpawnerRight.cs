﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerRight : MonoBehaviour
{

    public List<GameObject> Spawnables;
    public GameObject rightSpawner;

    public float Delay = 5.0f;

    private GameObject spawnedObject;

    private int randEnemy;

    private int counter = 0;




    // Use this for initialization
    void Start()
    {
        InvokeRepeating("Spawn", 0.0f, Delay);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Spawn()
    {
        float randY = Random.Range(-2.0f, 2.5f);

        randEnemy = Random.Range(0, 3);

        spawnedObject = Instantiate(Spawnables[randEnemy], new Vector3(rightSpawner.transform.position.x, rightSpawner.transform.position.y + randY, 0.0f), Quaternion.identity);

        counter++;
        if (counter > 5)
        {
            Delay -= 0.2f;
            counter = 0;
        }
    }

}