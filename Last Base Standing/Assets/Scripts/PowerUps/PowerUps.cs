﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour   
{
    //public float startingTime = 0.0f;
    bool crateOpened = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //if (crateOpened == true)
        //{
        //    startingTime -= Time.deltaTime;
        //    if (startingTime < 0)
        //    {
        //        crateOpened = false;
        //        startingTime = 7.0f;
        //    }
        //}
    }

    void OnMouseDown()
    {
        Destroy(gameObject);
        Debug.Log("REKT");

        crateOpened = true;

        int numberDrop = 0;
        numberDrop = Random.Range(0, 4);

        if (numberDrop == 0)
        {
            Debug.Log("BIGGER DAMAGE");
            if (crateOpened == true)
            {
                Weapon.instance.bulletDamage += 3.0f;
                Debug.Log("DAMAGE: " + Weapon.instance.bulletDamage);
            }
            //Weapon.instance.bulletDamage = Weapon.instance.defaultBulletDamage;
            Debug.Log("DAMAGE: " + Weapon.instance.bulletDamage);
        }

        else if (numberDrop == 1)
        {
            Debug.Log("FASTER BULLETS");
            if (crateOpened == true)
            {
                Weapon.instance.bulletSpeed += 0.3f;
                Debug.Log("BULLET SPEED: " + Weapon.instance.bulletSpeed);
            }
            //Weapon.instance.bulletSpeed = Weapon.instance.defaultBulletSpeed;
            Debug.Log("BULLET SPEED: " + Weapon.instance.bulletSpeed);
        }

        else if (numberDrop == 2)
        {
            Debug.Log("RAPID FIRE");
            if (crateOpened == true)
            {
                Weapon.instance.fireRate += 0.1f;
                Debug.Log("FIRE RATE: " + Weapon.instance.fireRate);
            }
            //Weapon.instance.fireRate = Weapon.instance.defaultFireRate;
            Debug.Log("FIRE RATE: " + Weapon.instance.fireRate);
        }

        else if (numberDrop == 3)
        {
            Debug.Log("HEALTH KIT");
            if (crateOpened == true)
            {
                Player.instance.playerHealth += 10.0f;
                Debug.Log("HEALTH: " + Player.instance.playerHealth);
            }
            //Weapon.instance.fireRate = Weapon.instance.defaultFireRate;
            Debug.Log("HEALTH: " + Player.instance.playerHealth);
        }
    }

}
