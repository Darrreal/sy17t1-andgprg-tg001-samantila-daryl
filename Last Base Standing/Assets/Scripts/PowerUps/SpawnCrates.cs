﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCrates : MonoBehaviour {

    public Transform[] spawnPoints;
    public float spawnTime = 1.0f;
    public float spawnDelay = 12.0f;

    public GameObject Crates;
	// Use this for initialization
	void Start ()
    {
        InvokeRepeating("SpawnCrate", spawnTime, spawnDelay);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void SpawnCrate()
    {
        int spawnIndex = Random.Range(0,spawnPoints.Length);
        Instantiate(Crates, spawnPoints[spawnIndex].position, spawnPoints[spawnIndex].rotation);
    }
}
